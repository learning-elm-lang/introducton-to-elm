# An Introduction to Elm

## Error Handling and Tasks

### Maybe

    > type Maybe a = Nothing | Just a

    > Nothing
    Nothing : Repl.Maybe a

    > Just
    <function> : a -> Repl.Maybe a

    > Just "hello"
    Just "hello" : Repl.Maybe String

    > Just 1.618
    Just 1.618 : Repl.Maybe Float

#### Optinal Fields

    type alias User =
        { name : String
        , age : Maybe Int
        }

Defining data with optional fields:

    sue : User
    sue =
        { name = "Sue", age = Nothing }

    tom : User
    tom =
        { name = "Tom", age = Just 24 }

Using data with optional fields:

    canBuyAlcohol : User -> Bool
    canBuyAlcohol user =
        case user.age of
            Nothing ->
                False

            Just age ->
                age >= 21

#### Partial Functions

    getTeenAge : User -> Maybe Int
    getTeenAge user =
        case user.age of
            Nothing ->
                Nothing

            Just age ->
                if 13 <= age && age <= 18 then
                    Just age
                else
                    Nothing

Sample usage:

    > type alias User =\
    |    { name : String, age : Maybe Int }

    > arno = { name = "Arno", age = Nothing }
    { name = "Arno", age = Nothing } : { age : Maybe.Maybe a, name : String }

    > lauri = { name = "Lauri", age = (Just 22) }
    { name = "Lauri", age = Just 22 } : { age : Maybe.Maybe number, name : String }

    > alisa = User "Alisa" (Just 21)
    { name = "Alisa", age = Just 21 } : Repl.User

    > karoliina = User "Karoliina" (Just 17)
    { name = "Karoliina", age = Just 17 } : Repl.User

    > users = [ arno, lauri, alisa, karoliina ]
    [{ name = "Arno", age = Nothing },{ name = "Lauri", age = Just 22 },{ name = "Alisa", age = Just 21 },{ name = "Karoliina", age = Just 17 }]
        : List Repl.User

    > getTeenAge user = \
    |     case user.age of \
    |         Nothing -> Nothing \
    |         Just age -> if 13 <= age && age <= 18 then Just age else Nothing
    <function> : { a | age : Maybe.Maybe number } -> Maybe.Maybe number1

    > List.filterMap getTeenAge users
    [17] : List number

### Result

    type Result error value
        = Err error
        | Ok value

Simple example:

    > import String

    > String.toInt "128"
    Ok 128 : Result.Result String Int

    > String.toInt "64"
    Ok 64 : Result.Result String Int

    > String.toInt "BBBB"
    Err "could not convert string 'BBBB' to an Int" : Result.Result String Int

### Task

tbd
