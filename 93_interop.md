# An Introduction to Elm

## Interop

### JSON

#### Primitive Decoders

    string : Decoder String
    int : Decoder Int
    float : Decoder Float
    bool : Decoder Bool

Decoders examples:

    > import Json.Decode exposing (..)

    > decodeString int "42"
    Ok 42 : Result.Result String Int

    > decodeString float "3.14159"
    Ok 3.14159 : Result.Result String Float

    > decodeString bool "true"
    Ok True : Result.Result String Bool

    > decodeString int "true"
    Err "Expecting an Int but instead got: true" : Result.Result String Int

#### Combining Decoders

    > import Json.Decode exposing (..)

    > int
    <decoder> : Json.Decode.Decoder Int

    > list int
    <decoder> : Json.Decode.Decoder (List Int)

    > decodeString (list int) "[1,2,3]"
    Ok [1,2,3] : Result.Result String (List Int)

    > decodeString (list string) """["hi", "yo"]"""
    Ok ["hi","yo"] : Result.Result String (List String)

    > decodeString (list (list int)) "[ [0], [1,2,3], [4,5] ]"
    Ok [[0],[1,2,3],[4,5]] : Result.Result String (List (List Int))

#### Decoding Objects

    > import Json.Decode exposing (..)

    > field "x" int
    <decoder> : Json.Decode.Decoder Int

    > decodeString (field "x" int) """{ "x": 3, "y": 4 }"""
    Ok 3 : Result.Result String Int

    > decodeString (field "y" int) """{ "x": 3, "y": 4 }"""
    Ok 4 : Result.Result String Int

Decoding using `map2` function:

    > import Json.Decode exposing (..)

    > type alias Point = { x : Int, y : Int }

    > Point
    <function> : Int -> Int -> Repl.Point

    > pointDecoder = map2 Point (field "x" int) (field "y" int)
    <decoder> : Json.Decode.Decoder Repl.Point

    > decodeString pointDecoder """{ "x": 3, "y": 4 }"""
    Ok { x = 3, y = 4 } : Result.Result String Repl.Point

### JavaScript Interop

#### Step 1: Embed in HTML

Following command compiles Elm source into `index.html` file:

    elm-make src/Main.elm

To compile Elm into JavaScript use following command:

    elm-make src/Main.elm --output=main.js

This JavaScript can be embedded into existing HTML page like this:

    <div id="main"></div>
    <script src="main.js"></script>
    <script>
        var node = document.getElementById('main');
        var app = Elm.Main.embed(node);
        // Note: if your Elm module is named "MyThing.Root" you
        // would call "Elm.MyThing.Root.embed(node)" instead.
    </script>

#### Step 2: Talk to JavaScript

##### Ports

    port module Spelling exposing (..)

    ...

    -- port for sending strings out to JavaScript
    port check : String -> Cmd msg

    -- port for listening for suggestions from JavaScript
    port suggestions : (List String -> msg) -> Sub msg

    ...

After running following command:

    elm-make Spelling.elm --output=spelling.js

It can be embedded in HTML like this:

    <div id="spelling"></div>
    <script src="spelling.js"></script>
    <script>
        var app = Elm.Spelling.fullscreen();

        app.ports.check.subscribe(function(word) {
            var suggestions = spellCheck(word);
            app.ports.suggestions.send(suggestions);
        });

        function spellCheck(word) {
            // have a real implementation!
            return [];
        }
    </script>

###### Flags

To get a value like this from JavaScript on initialization:

    type alias Flags =
        { user : String
        , token : String
        }

Set up Elm program like this:

    init : Flags -> ( Model, Cmd Msg )
    init flags =
        ...

    main =
        programWithFlags { init = init, ... }

And on the JavaScript side, start the program like this:

    // if you want it to be fullscreen
    var app = Elm.MyApp.fullscreen({
        user: 'Tom',
        token: '12345'
    });

    // if you want to embed your app
    var node = document.getElementById('my-app');
    var app = Elm.MyApp.embed(node, {
        user: 'Tom',
        token: '12345'
    });
