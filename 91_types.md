# An Introduction to Elm

## Types

### Reading Types

#### Primitives and Lists

Primitives:

    > "hello"
    "hello" : String

    > not True
    False : Bool

    > round 3.1415
    3 : Int

Lists:

    > ["Arno", "Lauri", "Alisa", "Karoliina"]
    ["Arno","Lauri","Alisa","Karoliina"] : List String

    > [1.0, 8.6, 42.1]
    [1,8.6,42.1] : List Float

    > []
    [] : List a

#### Functions

    > import String
    > String.length
    <function> : String -> Int

    > String.length "Supercalifragilisticexpialidocious"
    34 : Int

    > String.length [1,2,3]
    -- TYPE MISMATCH

    > tring.length True
    -- NAMING ERROR

#### Anonymous Functions

    > \n -> n / 2
    <function> : Float -> Float

    > (\n -> n / 2) 128
    64 : Float

#### Named Functions

    > oneHundredAndTwentyEight = 128.0
    128 : Float

    > half = \n -> n / 2
    <function> : Float -> Float

    > half oneHundredAndTwentyEight
    64 : Float

So this is:

    > half n = n / 2
    <function> : Float -> Float

Just a convenient shorthand for:

    > half = \n -> n / 2
    <function> : Float -> Float

Functions with multiple aguments:

    > divide x y = x / y
    <function> : Float -> Float -> Float

    > divide 3 2
    1.5 : Float

We can define methods in many ways:

    > divide x y = x / y
    <function> : Float -> Float -> Float

    > divide x = \y -> x / y
    <function> : Float -> Float -> Float

    > divide = \x -> (\y -> x / y)
    <function> : Float -> Float -> Float

This is how it is evaluated:

    divide 3 2
    (divide 3) 2                 -- Step 1 - Add the implicit parentheses
    ((\x -> (\y -> x / y)) 3) 2  -- Step 2 - Expand `divide`
    (\y -> 3 / y) 2              -- Step 3 - Replace x with 3
    3 / 2                        -- Step 4 - Replace y with 2
    1.5                          -- Step 5 - Do the math

step #3:

    > (\y -> 3 / y)
    <function> : Float -> Float

step #2:

    > (\x -> (\y -> x / y))
    <function> : Float -> Float -> Float

It is the same with all functions in Elm:

    > import String
    > String.repeat
    <function> : Int -> String -> String

It is possible to have partial application:

    > divide 128
    <function> : Float -> Float

    > String.repeat 3
    <function> : String -> String

#### Type Annotations

    half : Float -> Float
    half n =
        n / 2

    divide : Float -> Float -> Float
    divide x y =
        x / y

    askVegeta : Int -> String
    askVegeta powerLevel =
        if powerLevel > 9000 then
            "It's over 9000!!!"

        else
            "It is " ++ toString powerLevel ++ "."

### Type Aliases

    hasBio : { name : String, bio : String, pic : String } -> Bool
    hasBio user =
        String.length user.bio > 0

The code about can be simplified with type alias:

    type alias User =
        { name : String
        , bio : String
        , pic : String
        }

Now function can be defined in a much nicer way:

    hasBio : User -> Bool
    hasBio user =
        String.length user.bio > 0

    addBio : String -> User -> User
    addBio bio user =
        { user | bio = bio }

When defining record type alias constructor is generated:

    User : String -> String -> String -> User

    > type alias User = { name : String, bio : String, pic : String }
    > User "Tom" "Friendly Carpenter" "http://example.com/tom.jpg"
    { name = "Tom", bio = "Friendly Carpenter", pic = "http://example.com/tom.jpg" }
        : Repl.User

### Union Types

    > type Visibility = All | Active | Completed

    > All
    All : Repl.Visibility

    > Active
    Active : Repl.Visibility

    > Completed
    Completed : Repl.Visibility

#### Anonymous user example

    > type User = Anonymous | Named String

    > Anonymous
    Anonymous : Repl.User

    > Named
    <function> : String -> Repl.User

    > Named "AzureDiamond"
    Named "AzureDiamond" : Repl.User

    > Named "abraham-lincoln"
    Named "abraham-lincoln" : Repl.User

#### Widget Dashboard

    > type alias LogsInfo = \
    |     { logs : List String }

    > import Time exposing (..)
    > type alias TimeInfo = \
    |     { events : List (Time, Float) , yAxis : String }

    > type alias ScatterInfo = \
    |     { points : List (Float, Float), xAxis : String, yAxis : String }

    > type Widget = Logs LogsInfo | TimePlot TimeInfo | ScatterPlot ScatterInfo

    > Logs
    <function> : Repl.LogsInfo -> Repl.Widget

    > TimePlot
    <function> : Repl.TimeInfo -> Repl.Widget

    > ScatterPlot
    <function> : Repl.ScatterInfo -> Repl.Widget

#### Linked Lists

    > type IntList = Empty | Node Int IntList

    > Empty
    Empty : Repl.IntList

    > Node
    <function> : Int -> Repl.IntList -> Repl.IntList

    > Node 42 Empty
    Node 42 Empty : Repl.IntList

    > Node 64 (Node 128 Empty)
    Node 64 (Node 128 Empty) : Repl.IntList

Evaluation of linked list:

    sum (Node 1 (Node 2 (Node 3 Empty)))
    1 + sum (Node 2 (Node 3 Empty))
    1 + (2 + sum (Node 3 Empty))
    1 + (2 + (3 + sum Empty))
    1 + (2 + (3 + 0))
    1 + (2 + 3)
    1 + 5
    6

#### Generic Data Structures

    > type List a = Empty | Node a (List a)

    > Empty
    Empty : Repl.List a

    > Node
    <function> : a -> Repl.List a -> Repl.List a

    > Node "h1" Empty
    Node "h1" Empty : Repl.List String

    > Node 1.618 (Node 6.283 Empty)
    Node 1.618 (Node 6.283 Empty) : Repl.List Float

#### Additional Examples

##### Binary Tree

    > type Tree a = Empty | Node a (Tree a) (Tree a)

    > Node
    <function> : a -> Repl.Tree a -> Repl.Tree a -> Repl.Tree a

    > Node "h1" Empty Empty
    Node "h1" Empty Empty : Repl.Tree String

##### Languages

    type Boolean
        = T
        | F
        | Not Boolean
        | And Boolean Boolean
        | Or Boolean Boolean

    true = Or T F
    false = And T (Not T)
