import Optional as Opt exposing (Optional)

noService : Optional a -> Optional a -> Bool
noService shoes shirt =
    Optional.isNone shoes && Optional.isNone shirt
