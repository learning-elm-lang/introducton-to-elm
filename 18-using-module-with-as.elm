import Optional as Opt

noService : Opt.Optional a -> Opt.Optional a -> Bool
noService shoes shirt =
    Optional.isNone shoes && Optional.isNone shirt
