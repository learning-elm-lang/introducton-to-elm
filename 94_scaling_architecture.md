# An Introduction to Elm

## Scaling The Elm Architecture

### Labeled Checkboxes

To create some HTML like this:

    <fieldset>
        <label><input type="checkbox">Email Notifications</label>
        <label><input type="checkbox">Video Autoplay</label>
        <label><input type="checkbox">Use Location</label>
    </fieldset>

Implementation can be found here [14-checkboxes.elm](14-checkboxes.elm).

### Radio Buttons

We want some HTML like this:

    <fieldset>
        <label><input type="radio">Small</label>
        <label><input type="radio">Medium</label>
        <label><input type="radio">Large</label>
    </fieldset>

Implementation can be found here [15-radio.elm](15-radio.elm).

### Modules

#### Defining Modules

    module Optional exposing (..)

    type Optional a = Some a | None

    isNone : Optional a -> Bool
    isNone optional =
        case optional of
            Some _ ->
                False

            None ->
                True

This module exposes everything. To expose only specific values, do like that:

    module Optional exposing ( Optional(..), isNone )

#### Using Modules

    import Optional

    noService : Optional.Optional a -> Optional.Optional a -> Bool
    noService shoes shirt =
        Optional.isNone shoes && Optional.isNone shirt

##### As

    import Optional as Opt

    noService : Opt.Optional a -> Opt.Optional a -> Bool
    noService shoes shirt =
        Optional.isNone shoes && Optional.isNone shirt

It can be done even like this:

    import Facebook.News.Story as Story

##### Exposing

    import Optional exposing (Optional)

    noService : Optional a -> Optional a -> Bool
    noService shoes shirt =
        Optional.isNone shoes && Optional.isNone shirt

##### Mixing Both

    import Optional as Opt exposing (Optional)

    noService : Optional a -> Optional a -> Bool
    noService shoes shirt =
        Optional.isNone shoes && Optional.isNone shirt

#### Building Projects with Multiple Modules

Every Elm project has an elm-package.json file at its root:

    {
        "version": "1.0.0",
        "summary": "helpful summary of your project, less than 80 characters",
        "repository": "https://github.com/user/project.git",
        "license": "BSD3",
        "source-directories": [
            "src",
            "benchmarks/src"
        ],
        "exposed-modules": [],
        "dependencies": {
            "elm-lang/core": "4.0.2 <= v < 5.0.0",
            "elm-lang/html": "1.1.0 <= v < 2.0.0"
        },
        "elm-version": "0.17.0 <= v < 0.18.0"
    }

Typically, you will say `"source-directories": [ "src" ]` and have your project set up like this:

    my-project/elm-package.json
    my-project/src/Main.elm
    my-project/src/Optional.elm

To compile your `Main.elm` file:

    cd my-project
    elm-make src/Main.elm
