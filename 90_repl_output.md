# An Introduction to Elm

## Core language

### Values

    > "hello"
    "hello" : String
    > "hello" ++ "world"
    "helloworld" : String
    > "hello" ++ " world"
    "hello world" : String

    > 2 + 3 * 4
    14 : number
    > (2 + 3) * 4
    20 : number

    > 9 / 2
    4.5 : Float
    > 9 // 2
    4 : Int

### Functions

    > isNegative n = n < 0
    <function> : number -> Bool

    > isNegative 4
    False : Bool

    > isNegative -7
    True : Bool

    > isNegative (-3 * -4)
    False : Bool

### If Expressions

    > if True then "hello" else "world"
    "hello" : String

    > if False then "hello" else "world"
    "world" : String

    > over9000 powerLevel = \
    |   if powerLevel > 9000 then "It's over 9000!!!" else "meh"
    <function> : number -> String

    > over9000 2
    "meh" : String

    > over9000 10000
    "It's over 9000!!!" : String

### Lists

    > names = [ "Alice", "Bob", "Chuck" ]
    ["Alice","Bob","Chuck"] : List String

    > List.isEmpty names
    False : Bool

    > List.length names
    3 : Int

    > List.reverse names
    ["Chuck","Bob","Alice"] : List String

    > numbers = [1,4,3,2]
    [1,4,3,2] : List number

    > List.sort numbers
    [1,2,3,4] : List number

    > double n = n * 2
    <function> : number -> number

    > List.map double numbers
    [2,8,6,4] : List number

### Tuples

    > import String
    > goodName name = \
    |   if String.length name <= 20 then \
    |     (True, "name accepted!") \
    |   else \
    |     (False, "name was too long; please limit it to 20 characters")
    <function> : String -> ( Bool, String )

    > goodName "Edu"
    (True,"name accepted!") : ( Bool, String )

    > goodName "BlahBlahBlahBlahBlahBlah"
    (False,"name was too long; please limit it to 20 characters") : ( Bool, String )

### Records

    > point = { x = 3, y = 4 }
    { x = 3, y = 4 } : { x : number, y : number1 }

    > point.x
    3 : number

    > steve = { name =  "Jobs", age = 56 }
    { name = "Jobs", age = 56 } : { age : number, name : String }

    > steve.name
    "Jobs" : String

    > .name steve
    "Jobs" : String

    > List.map .name [steve,steve,steve]
    ["Jobs","Jobs","Jobs"] : List String

    > under70 {age} = age < 70
    <function> : { a | age : number } -> Bool

    > under70 steve
    True : Bool

    > under70 { species = "Triceratops", age = 68000000 }
    False : Bool

    > { steve | name = "Nye" }
    { name = "Nye", age = 56 } : { age : number, name : String }

    > { steve | age = 22 }
    { name = "Jobs", age = 22 } : { name : String, age : number }

## The Elm Architecture

### User Input

To get examples running start `elm-reactor`:

    $ elm-reactor

Then navigate to http://localhost:8000

## Types

[Elm types in more details](91_types.md).

## Error Handling and Tasks

[Error handling in Elm](92_error_handling.md).

## Interop

[Interop between Elm and JavaScript](93_interop.md).

## Scaling The Elm Architecture

[Scaling The Elm Architecture](94_scaling_architecture.md).
