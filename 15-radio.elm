import Html exposing (Html, Attribute, div, fieldset, input, label, text, section)
import Html.Attributes exposing (name, style, type_)
import Html.Events exposing (onClick)

main =
    Html.beginnerProgram { model = chapter1, update = update, view = view }

-- MODEL
type alias Model =
    { fontSize: FontSize
    , content: String
    }

type FontSize = Small | Medium | Large

chapter1 : Model
chapter1 =
    Model Medium intro

intro : String
intro = "Happy families are all alike; every unhappy family is unhappy in its own way."

-- UPDATE
type Msg = SwitchTo FontSize

update : Msg -> Model -> Model
update msg model =
    case msg of
        SwitchTo newFontSize ->
            { model | fontSize = newFontSize }

-- VIEW
view : Model -> Html Msg
view model =
    div []
        [ fieldset []
            [ radio (SwitchTo Small) "Small"
            , radio (SwitchTo Medium) "Medium"
            , radio (SwitchTo Large) "Large"
            ]
        , section [] [ text model.content ]
        ]

radio : msg -> String -> Html msg
radio msg name =
    label [ style [("padding", "20px")] ]
    [ input [ type_ "radio", onClick msg ] []
    , text name
    ]
