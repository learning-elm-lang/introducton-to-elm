import Html exposing (text)
import Time exposing (..)

type alias LogsInfo =
    { logs : List String
    }

type alias TimeInfo =
    { events : List (Time, Float)
    , yAxis : String
    }

type alias ScatterInfo =
    { points : List (Float, Float)
    , xAxis : String
    , yAxis : String
    }

viewLogs : LogsInfo -> Html msg
viewLogs info =
    div []
    [
        div [] [text "Log Info: "]
        div [] [text info.logs]
    ]

viewTime : TimeInfo -> Html msg
viewTime info ->
    div []
    [
        div [] [text "Time info: "]
        div [] [text info.yAxis]
    ]

viewScatter : ScatterInfo -> Html msg
viewScatter info =
    div [] [
        div [] [text "Scatter info:"]
    ]

type Widget = Logs LogsInfo | TimePlot TimeInfo | ScatterPlot ScatterInfo

view : Widget -> Html Msg
view widget =
    case widget of
        Logs info ->
            viewLogs info

        TimePlot info ->
            viewTime info

        ScatterPlot info ->
            viewScatter info