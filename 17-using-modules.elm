import Optional

noService : Optional.Optional a -> Optional.Optional a -> Bool
noService shoes shirt =
    Optional.isNone shoes && Optional.isNone shirt
