import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput)
import String exposing (..)

main =
    Html.beginnerProgram { model = model, view = view, update = update }

-- MODEL
type alias Model =
    { name: String
    , password: String
    , passwordAgain: String
    , age: String
    }

model : Model
model =
    Model "" "" "" ""

-- UPDATE
type Msg
    = Name String
    | Password String
    | PasswordAgain String
    | Age String

update : Msg -> Model -> Model
update msg model =
    case msg of
        Name name -> 
            { model | name = name }
        Password password -> 
            { model | password = password }
        PasswordAgain password -> 
            { model | passwordAgain = password }
        Age age ->
            { model | age = age }

-- VIEW
view : Model -> Html Msg
view model =
    div []
        [ input [ type_ "text", placeholder "Name", onInput Name] []
        , input [ type_ "password", placeholder "Password", onInput Password] []
        , input [ type_ "password", placeholder "Re-enter Password", onInput PasswordAgain] []
        , input [ type_ "number", onInput Age] []
        , viewValidation model
        ]

viewValidation : Model -> Html msg
viewValidation model =
    let 
        (color, message) = 
            if length model.password < 8 then
                ("red", "Password must be onger than 8 characters")
            else if model.password == model.passwordAgain then
                ("green", "OK")
            else
                ("red", "Password do not match")
    in
        div [ style [("color", color)] ] [ text message ]


-- Exercises:
-- * Check that the password is longer than 8 characters.
-- * Make sure the password contains upper case, lower case, and numeric characters.
-- * Add an additional field for age and check that it is a number.
-- * Add a "Submit" button. Only show errors after it has been pressed.